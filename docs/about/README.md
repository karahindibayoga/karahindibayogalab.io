---
sidebar: false
title: Hakkımda
---

# Hakkımda

Betûl Bitir Soylu, İstanbul Üniversitesi Moleküler Biyoloji ve Genetik Bölümü'nden 2011 yılında mezun oldu. Özel bir laboratuvarın moleküler biyoloji bölümünde 3 yıl boyunca çalıştı. Genetiği Değiştirilmiş Organizmaların tespiti üzerine çalışmalar yaptı. İstanbul Üniversitesi Adli Bilimler Enstitüsü'nden yüksek lisansını aldı.  Ekoloji, permakültür, gıda güvenliği, biyoçeşitliliğin korunması ile yakından ilgileniyor.

Yoga ile tanışması 2012 yılında oldu. Bu çalışmaların bedenine ve zihnine çok iyi geldiğini farketti. Daha sonra çalışma hayatı devam ederken yoga merkezlerinde yoga çalışmalarına katılmaya başladı. Düzenli yoga pratiğinin olumlu etkileri ve bu süreçteki deneyimleri yogaya olan ilgisini arttırdı. 2018 yılında kurumsal iş hayatından ayrılıp yoga ve tiyatro çalışmalarına yöneldi.
Hara Yoga Vedanta Center'da Çocuk Yogası uzmanlığı alıp burada ve farklı merkezlerde çocuklarla yoga yapmaya başladı.
Ayrıca meditasyonun Batı'daki yansıması diyebileceğimiz bilinçli farkındalık çalışmalarını daha iyi anlayabilmek ve çocuk çalışmalarına katkı sağlaması için Sepin İnceer'in 9 haftalık Ebeveynler ve Eğitmenler için Mindfulness programına katıldı. Bu eğitim ile Eline Snel'in geliştirmiş olduğu metot olan MIE (Mindfulness in Education) ile 4-16 yaş arasındaki çocuklarla çalışmalar üzerine yaklaşımları deneyimledi.
Yogada derinleşmek için Damla Dönmez'in 200 saatlik yoga uzmanlık programına katıldı. Bu eğitim sayesinde yoga yolculuğunda yeni bir kapı açıldı. Kendisine doğru çıktığı bu yolculukta şimdi öğrendiklerini ve deneyimlediklerini başkalarıyla paylaşıyor.
Yogapan'da çocuk ve yetişkinler için yoga çalışmaları yapıyor.

Yoganın önce kendine sonra karşılaştığı insanlara şifa olması dileğiyle çalışmalarına devam ediyor.

## Eğitimler

* Yoga Uzmanlık Eğitimi: [Damla Dönmez ile Yoga Uzmanlık Programı](https://www.damladonmez.com/uzmanlik/)
* Çocuk Yogası Uzmanlık Eğitimi: [Hara Yoga Vedanta Center](http://www.harayoga.com)
* Ebeveynler ve Eğitmenler için Mindfulness Eğitimi: [Sepin İnceer ile Mindfulness](https://sepininceer.com/yetiskinler-icin-mindfulness-egitimi-mie/)
