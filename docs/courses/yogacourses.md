---
title: Yoga Kursları
---

# Yoga Kursları

## Yoga'ya Başlangıç Kursu

Geleneksel Yoga uygulamaları, kişinin beden, zihin ve nefes birlikteliğini sağlaması ve bunların arasındaki uyumu geliştirmeyi hedefleyen yöntemlerdir. Yoga, dengeli, daha saf ve sağlıklı bir beden ve zihne ulaşmak için bütüncül bir yaklaşım ortaya koyar.

Geleneksel Hatha Yoga'ya giriş niteliğinde sayılan bu Başlangıç Kursu'nda Yoga'nın felsefesi ve ana yapısı, geleneksel bir ekol olarak bilinen Şivananda Yoga'nın temel asanaları (duruşları), nefes ve gevşeme teknikleri sırasıyla öğretilecektir.

Yoga yaş, beden ve tecrübe farketmeksizin herkes tarafından uygulanabilir. Öğretmen kişiye doğru uygulama için rehber olarak öğrencinin kendine uygun pratikle uygulamadan en fazla faydayı elde etmesine yardımcı olur.

#### Kurs İçeriği:

##### Pratik

* Doğru Nefes Kullanımı & Temel Nefes Teknikleri
* Surya Namaskar (Güneşe Selam)
* Şivananda Yoga'nın 12 Temel Pozu
* Gevşeme Teknikleri

##### Teori

* Yoga Nedir, Neden?
* Yogik Yaşam Tarzı
* Doğru Egzersiz Nasıl Olmalı?
* Doğru Nefes Nasıl Alınmalı?
* Doğru Gevşeme Nedir?
* Doğru Beslenme Nasıl Olmalı?
* Olumlu Düşünce ve Meditasyon
