---
title: Çocuk Yogası
---

# Çocuk Yogası

## Çocuk Yogası (4-8 Yaş)

Çocuk yogası, yoga ve oyunun karışımıdır. Nefes çalışmaları, hikayeler, grup oyunları, yönlendirmeli meditasyon ve pek çok farklı yolla çocukların yogayı deneyimlemesini sağlar. Yoga doğadan ilham alır, bu sebeple çocuk yogası sık sık doğayı taklit yoluyla çeşitli figürlerle gerçekleştirilir. Çocuklarda denge, odaklanma ve gevşeme yetilerini geliştirir. Bu tarz çalışmalar ile çocuklar erken yaşlarda beden farkındalığı geliştirirken aynı zamanda duygularını ifade için uygun bir alan bulurlar ve böylece gelişimlerinde kendilerini keşif yoluna çıkarlar. Zaten esnek olan bedenlerini kullanmayı öğrenirler. Ayrıca kendi yaş gruplarında diğer çocuklarla sosyalleşme ve paylaşım fırsatı bulurlar. Yoga derslerinde zorlama ve rekabet içeren etkinlikler yer almaz. Çocuklar ihtiyaçlarının farkına varmayı, kendileriyle ve diğerleriyle uyum içinde olmayı deneyimlerler. Bir yandan sorumluluk duygusu geliştirilirken diğer yandan eğlence öğesi eksik olmaz.

Bu etkinliği, kendinin ve duygularının farkında, dengeli bir beden ve zihne sahip bireyler olma yolunda minik ama önemli adımlardan biri olarak görüyoruz.

#### İçerik:

* Nefes Pratikleri
* Yoga Duruşları
* Hikayelerle Yoga
* Eşli Pozlarla Yoga
* Yönlendirmeli Meditasyon
* Gevşeme Pratiği
