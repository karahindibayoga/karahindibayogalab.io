---
sidebar: false
title: İletişim
---

# İletişim

Benimle iletişime geçmek ve daha fazla bilgi almak için <karahindibayoga@gmail.com> adresine e-posta gönderebilirsiniz.

Ya da [Instagram](https://www.instagram.com/karahindibapuf/)'dan iletişime geçebilirsiniz.

