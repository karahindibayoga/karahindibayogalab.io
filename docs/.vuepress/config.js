const {defaultTheme} = require("vuepress");

module.exports = {
    // site config
    base: '/',

    head: [
        ['link', { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/images/icons/favicon-16x16.png' }],
        ['link', { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/images/icons/favicon-32x32.png' }],
        ['link', { rel: 'manifest', href: '/manifest.webmanifest' }],
        ['meta', { name: 'application-name', content: 'Karahindiba Yoga' }],
        ['meta', { name: 'apple-mobile-web-app-title', content: 'Karahindiba Yoga' }],
        [
            'meta',
            { name: 'apple-mobile-web-app-status-bar-style', content: 'black' },
        ],
        [
            'link',
            { rel: 'apple-touch-icon', href: `/images/icons/apple-touch-icon.png` },
        ],
        [
            'link',
            {
                rel: 'mask-icon',
                href: '/images/icons/safari-pinned-tab.svg',
                color: '#e98d45',
            },
        ],
        ['meta', { name: 'msapplication-TileColor', content: '#e98d45' }],
        ['meta', { name: 'theme-color', content: '#e98d45' }],
    ],
    locales: {
        // The key is the path for the locale to be nested under.
        // As a special case, the default locale can use '/' as its path.
        '/': {
            lang: 'tr-TR',
            title: 'Karahindiba Yoga',
            description: 'Betûl Bitir Soylu',

        },
/*        '/en/': {
            lang: 'en-US',
            title: 'Karahindiba Yoga',
            description: 'Betûl Bitir Soylu',

        },*/
    },

    // theme and its config
    theme: defaultTheme({
        logo: 'images/hero.png',
        contributors: false,
        editLink: false,
        locales: {
            '/': {
                selectLanguageName: 'Türkçe',
                toggleDarkMode: 'karanlık modu aç/kapat',
                lastUpdated: false,
                lastUpdatedText: 'Son Güncelleme',
                notFound: ['Böyle bir sayfa bulunamadı.'],
                backToHome: 'Anasayfaya geri dön',
                openInNewWindow: 'Yeni pencerede aç',
                navbar: [
                    {
                        text: 'Anasayfa',
                        link: '/',
                    },
                    {
                        text: 'Hakkımda',
                        link: '/about/',
                    },
                    {
                        text: 'Yoga',
                        link: '/yoga/',
                    },
                    {
                        text: 'Eğitimler',
                        children: [
                            {
                                text: 'Eğitimler',
                                children: [
                                    '/courses/yogacourses.md',
                                    '/courses/kidsyoga.md',
                                    '/courses/drama.md',
                                    '/courses/mindfulness.md',
                                ],
                            },
                        ]
                    },
                    {
                        text: 'Galeri',
                        link: '/gallery/',
                    },
/*                    {
                        text: 'Blog',
                        link: '/blog/',
                    },*/
                    {
                        text: 'İletişim',
                        link: '/contact/',
                    }
                ],
                sidebar: {
                    '/courses/': [
                        {
                            text: 'Eğitimler',
                            children: [
                                '/courses/yogacourses.md',
                                '/courses/kidsyoga.md',
                                '/courses/drama.md',
                                '/courses/mindfulness.md',
                            ],
                        },
                    ],
/*                    '/blog/': [
                        {
                            text: 'Blog',
                            children: [
                                '/blog/README.md',
                            ],
                        },
                    ],*/
                },
            },
            '/en/': {
                selectLanguageName: 'English',
                lastUpdated: false,
                navbar: [
                    {
                        text: 'Home',
                        link: '/en/',
                    },
                    {
                        text: 'About me',
                        link: '/en/about/',
                    },
                    {
                        text: 'Yoga',
                        link: '/en/yoga/',
                    },
                    {
                        text: 'Courses',
                        children: [
                            {
                                text: 'Courses',
                                children: [
                                    '/en/courses/yogacourses.md',
                                    '/en/courses/kidsyoga.md',
                                    '/en/courses/drama.md',
                                    '/en/courses/mindfulness.md',
                                ],
                            },
                        ]
                    },
                    {
                        text: 'Gallery',
                        link: '/en/gallery/',
                    },
/*                    {
                        text: 'Blog',
                        link: '/en/blog/',
                    },*/
                    {
                        text: 'Contact',
                        link: '/en/contact/',
                    }
                ],
                sidebar: {
                    '/en/courses/': [
                        {
                            text: 'Courses',
                            children: [
                                '/en/courses/yogacourses.md',
                                '/en/courses/kidsyoga.md',
                                '/en/courses/drama.md',
                                '/en/courses/mindfulness.md',
                            ],
                        },
                    ],
/*                    '/en/blog/': [
                        {
                            text: 'Blog',
                            children: [
                                '/en/blog/README.md',
                            ],
                        },
                    ],*/
                },
            },
        },
    })
}
