# karahindibayoga

Vuepress project, deployed with GitLab Pages.

## Installation

- Initialize project
```bash
yarn init
```
- Install [VuePress](https://v2.vuepress.vuejs.org/)
```bash
yarn add -D vuepress@next
```

## Usage

- Run in the local server
```bash
yarn docs:dev
```

## License
MIT Licensed.
